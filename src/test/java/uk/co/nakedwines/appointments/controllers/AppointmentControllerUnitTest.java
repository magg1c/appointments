package uk.co.nakedwines.appointments.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.support.BindingAwareModelMap;
import uk.co.nakedwines.appointments.entities.Appointment;
import uk.co.nakedwines.appointments.repositories.AppointmentRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class AppointmentControllerUnitTest {

    private AppointmentsController userController;
    private AppointmentRepository mockedUserRepository;
    private BindingResult mockedBindingResult;
    private Model model;

    private List<Appointment> appointmentList;

    @Before
    public void beforeEachTest() {
        mockedUserRepository = mock(AppointmentRepository.class);
        mockedBindingResult = mock(BindingResult.class);
        model = new BindingAwareModelMap();
        userController = new AppointmentsController(mockedUserRepository);
        appointmentList = new ArrayList<>();

        when(mockedUserRepository.findAllByOrderByDateDesc()).thenReturn(appointmentList);
    }

    @Test
    public void whenCallingIndex_thenReturnRedirectToAppointments() {

        assertThat(userController.index()).isEqualTo("redirect:/appointments");
    }

    @Test
    public void whenCallingGetAppointments_thenTheAppointmentsListShouldBePopulated() {

        userController.getAppointments(model);

        assertThat(model.getAttribute("appointments")).isEqualTo(appointmentList);
    }

    @Test
    public void whenCallingGetAppointments_thenTheAppointmentShouldBePopulated() {

        userController.getAppointments(model);

        assertThat(model.getAttribute("appointment").toString())
                        .isEqualTo((new Appointment()).toString());
    }

    @Test
    public void whenCallingAddAppointmentWithValidBindingResult_thenTheAppointmentRepositoryShouldInvokeSave() {

        Appointment appointment = new Appointment();
        appointment.setDate(LocalDate.now());
        appointment.setDescription("Test Message");

        userController.addAppointment(appointment, mockedBindingResult, model);

        verify(mockedUserRepository, times(1)).save(appointment);
    }

    @Test
    public void whenCallingAddAppointmentWithInvalidBindingResult_thenTheAppointmentRepositoryShouldNotInvokeSave() {

        Appointment appointment = new Appointment();
        when(mockedBindingResult.hasErrors()).thenReturn(true);

        userController.addAppointment(appointment, mockedBindingResult, model);

        verify(mockedUserRepository, times(0)).save(appointment);
    }

    @Test
    public void whenCallingAddAppointmentWithInvalidBindingResult_thenTheReturnTypeShouldBeAppointments() {

        Appointment appointment = new Appointment();
        when(mockedBindingResult.hasErrors()).thenReturn(true);

        String response = userController.addAppointment(appointment, mockedBindingResult, model);

        assertThat(response).isEqualTo("appointments");
    }

    @Test
    public void whenCallingAddAppointmentWithInvalidBindingResult_thenTheModelShouldHaveAppointmentsPopulated() {

        Appointment appointment = new Appointment();
        when(mockedBindingResult.hasErrors()).thenReturn(true);

        userController.addAppointment(appointment, mockedBindingResult, model);

        assertThat(model.getAttribute("appointments")).isEqualTo(appointmentList);
    }
}
