package uk.co.nakedwines.appointments.controllers;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppointmentControllerIntegrationTest {

    @Autowired
    private AppointmentsController controller;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void rootReturnTheAppointmentsPage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/", String.class))
                                     .contains("<title>Naked Java Appointments</title>");
    }

    @Test
    public void appointmentsPage() throws Exception {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/appointments", String.class))
                .contains("<title>Naked Java Appointments</title>");
    }

    @Test
    public void createAppointmentThrowsError_WhenNoAppointmentDateOrDescriptionSupplied() throws Exception {
        String resp = this.restTemplate.postForObject("http://localhost:" + port + "/appointments/create", null, String.class);

        assertThat(resp).contains("An Appointment date must be Supplied");
        assertThat(resp).contains("Description is mandatory");
    }

    @Test
    public void createAppointmentThrowsError_WhenAppointmentDateInThePast() throws Exception {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("date", LocalDate.now().minusDays(1).toString());
        map.add("description","aaaaa");

        String resp = this.restTemplate.postForObject("http://localhost:" + port + "/appointments/create", getEntity(map), String.class);

        assertThat(resp).contains("Appointment date must be today or in the future");
    }

    @Test
    public void createAppointmentThrowsError_WhenDescriptionIsOnly2Characters() throws Exception {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("date", LocalDate.now().toString());
        map.add("description","aa");

        String resp = this.restTemplate.postForObject("http://localhost:" + port + "/appointments/create", getEntity(map), String.class);

        assertThat(resp).contains("Description must be at least 3 characters");
    }

    @Test
    public void createAppointmentThrowsError_WhenDescriptionIsMoreThan50Characters() throws Exception {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("date", LocalDate.now().toString());
        map.add("description","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pharetra ornare risus quis tempus. Donec faucibus pellentesque urna. Etiam consequat elit nec lorem cursus, laoreet maximus sapien bibendum. Pellentesque vel ullamcorper lectus. Aliquam erat volutpat. Sed non leo condimentum, finibus mauris ut, facilisis tortor. Fusce sit amet auctor eros, in hendrerit lorem. Maecenas vestibulum at ipsum laoreet pulvinar. Fusce tempor iaculis dui nec sagittis.");

        String resp = this.restTemplate.postForObject("http://localhost:" + port + "/appointments/create", getEntity(map), String.class);

        assertThat(resp).contains("Description exceeds 50 characters");
    }

    //Edge Case, should only happen if POST request directly made
    @Test
    public void createAppointmentThrowsError_WhenIncorrectDateFormatSupplied() throws Exception {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("date","30/09/2020");
        map.add("description","aaaaa");

        String resp = this.restTemplate.postForObject("http://localhost:" + port + "/appointments/create", getEntity(map), String.class);

        assertThat(resp).contains("Parse attempt failed for value [30/09/2020]");
    }

    private HttpEntity<MultiValueMap<String, String>> getEntity(MultiValueMap<String, String> map) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        return new HttpEntity<>(map, headers);
    }

}
