package uk.co.nakedwines.appointments.entities;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AppointmentUnitTest {

    private Validator validator;
    private Appointment appointment;

    @Before
    public void beforeEachTest() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();

        appointment = new Appointment();
        appointment.setDescription("Valid Description");
        appointment.setDate(LocalDate.now());
    }

    @Test
    public void whenCallingValidatorWithValidAppointment_thenViolationsShouldBeEmpty() {

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);
        assertTrue(violations.isEmpty());
    }

    @Test
    public void whenCallingValidatorWithEmptyDescription_thenViolationShouldOccur() {

        appointment.setDescription(null);

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertThat(violations.iterator().next().getMessage()).isEqualTo("{appointment.field.description.error.empty}");
    }

    @Test
    public void whenCallingValidatorWithVeryLongDescription_thenViolationShouldOccur() {

        appointment.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pharetra ornare risus quis tempus. Donec faucibus pellentesque urna. Etiam consequat elit nec lorem cursus, laoreet maximus sapien bibendum. Pellentesque vel ullamcorper lectus. Aliquam erat volutpat. Sed non leo condimentum, finibus mauris ut, facilisis tortor. Fusce sit amet auctor eros, in hendrerit lorem. Maecenas vestibulum at ipsum laoreet pulvinar. Fusce tempor iaculis dui nec sagittis.");

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertThat(violations.iterator().next().getMessage()).isEqualTo("{appointment.field.description.error.maxlength}");
    }

    @Test
    public void whenCallingValidatorWithVeryShortDescription_thenViolationShouldOccur() {

        appointment.setDescription("Lo");

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertThat(violations.iterator().next().getMessage()).isEqualTo("{appointment.field.description.error.minlength}");
    }

    @Test
    public void whenCallingValidatorWithEmptyDate_thenViolationShouldOccur() {

        appointment.setDate(null);

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertThat(violations.iterator().next().getMessage()).isEqualTo("{appointment.field.date.error.empty}");
    }

    @Test
    public void whenCallingValidatorWithPastDate_thenViolationShouldOccur() {

        appointment.setDate(LocalDate.now().minusDays(1));

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertThat(violations.iterator().next().getMessage()).isEqualTo("{appointment.field.date.error.past}");
    }

    @Test
    public void whenCallingValidatorWithTodaysDate_thenViolationShouldNotOccur() {

        appointment.setDate(LocalDate.now());

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertTrue(violations.isEmpty());
    }

    @Test
    public void whenCallingValidatorWithTomorrowsDate_thenViolationShouldNotOccur() {

        appointment.setDate(LocalDate.now().plusDays(1));

        Set<ConstraintViolation<Appointment>> violations = validator.validate(appointment);

        assertTrue(violations.isEmpty());
    }
}
