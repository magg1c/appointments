package uk.co.nakedwines.appointments.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import uk.co.nakedwines.appointments.entities.Appointment;
import uk.co.nakedwines.appointments.repositories.AppointmentRepository;

import javax.validation.Valid;

@Controller
public class AppointmentsController {

    private final AppointmentRepository appointmentRepository;

    @Autowired
    public AppointmentsController(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/appointments";
    }

    @GetMapping("/appointments")
    public String getAppointments(Model model) {

        model.addAttribute("appointments", appointmentRepository.findAllByOrderByDateDesc());
        model.addAttribute("appointment", new Appointment());

        return "appointments";
    }

    @PostMapping("/appointments/create")
    public String addAppointment(@Valid Appointment appointment, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("appointments", appointmentRepository.findAllByOrderByDateDesc());
            return "appointments";
        }

        appointmentRepository.save(appointment);

        return "redirect:/appointments";
    }

}
