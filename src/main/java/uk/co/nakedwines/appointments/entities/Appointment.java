package uk.co.nakedwines.appointments.entities;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message = "{appointment.field.description.error.empty}")
    @Length(min = 3, message = "{appointment.field.description.error.minlength}")
    @Length(max = 50, message = "{appointment.field.description.error.maxlength}")
    private String description;

    @FutureOrPresent(message = "{appointment.field.date.error.past}")
    @NotNull(message = "{appointment.field.date.error.empty}")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + this.id + "\"," +
                "\"description\": \"" + this.description + "\"," +
                "\"date\": \"" + this.date + "\"" +
               "}";
    }

}