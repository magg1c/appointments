package uk.co.nakedwines.appointments.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import uk.co.nakedwines.appointments.entities.Appointment;

import java.util.List;

@Repository
public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

    List<Appointment> findAllByOrderByDateDesc();

}
