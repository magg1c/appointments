$(function () {
    $('#appointment-date').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy'
    });

    $('#appointment-date').on('changeDate', function() {
        let isoFormattedDate = $('#display-date').val().split('/').reverse().join('-');
        $('#date').val(isoFormattedDate);
    });
});